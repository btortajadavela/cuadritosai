#include <stdio.h>
#include <stdlib.h>
#include "init.c"

int main(int argc, char* argv[]) {
    if (argc==1){
      FILE* stream;
      if((stream=fopen(fileName, "wb"))==NULL){
        printf("Failed to open the file.\n");
        fclose(stream);
        return 1;
      }
      for (int i=0; i<SIZE; i++){
        for (int j=0; j<SIZE; j++){
          if((fwrite(&defaultTile, sizeof(tile), 1, stream))==0){
            printf("Failed to write the file.\n");
            return 1;
          }
        }
      }
      return 0;
    } else if (argc==5){
      FILE* stream;
      if((stream=fopen(fileName, "rb"))==NULL){
        printf("Failed to open the file.\n");
        fclose(stream);
        return 1;
      }
      if ((BOARD = (tile**) malloc (SIZE*sizeof(tile*))) == NULL){
        printf("Failed to allocate memory.\n");
        return 1;
      }
      for (int i=0; i<SIZE; i++){
        if ((*(BOARD+i) = (tile*) malloc (SIZE*sizeof(tile))) == NULL){
          printf("Failed to allocate memory.\n");
          return 1;
        }
      }
      for (int i=0; i<SIZE; i++){
        for (int j=0; j<SIZE; j++){
          if((fread((*(BOARD+i)+j), sizeof(tile), 1, stream))==0){
            printf("Failed to read the file.\n");
            return 1;
          }
        }
      }
      int cursor[]={atoi(argv[1]), atoi(argv[2]), atoi(argv[3])};
      int turn=atoi(argv[4]);

      placeWall(cursor[0], cursor[1], cursor[2], BOARD);
      int retVal = colorTheBoard(cursor[0], cursor[1], turn, BOARD);

      fclose(stream);
      if((stream=fopen(fileName, "wb"))==NULL){
        printf("Failed to open the file.\n");
        fclose(stream);
        return 1;
      }
      //system("clear");
      for (int i=0; i<SIZE; i++){
        for (int j=0; j<SIZE; j++){
          //printf("%d\t", (*(BOARD+j)+i)->value);
          if((fwrite((*(BOARD+i)+j), sizeof(tile), 1, stream))==0){
            printf("Failed to write the file.\n");
            return 1;
          }
        }
      }
      fclose(stream);
      return retVal;
    }
    printf("Four arguments were expected:\nCol\tRow\tWall\tTurn\n\nExiting the script...\n");
    return 1;
}
