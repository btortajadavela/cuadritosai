#include <stdio.h>
#include <stdlib.h>

#define SIZE 5
#define PLAYERS 2

typedef struct{
    short top;
    short right;
    short bottom;
    short left;
    short last;
    short value;
    short color;
} tile;

int placeWall (int, int, int, tile**);
int colorTheBoard(int, int, int, tile**, char** grid, int* TOTAL);
int colorNeighbour(int, int, int, tile**, char** grid, int* TOTAL);
int initGrid(char**);
int putGrid(int, int, int, char**);
int printGrid(char**);
