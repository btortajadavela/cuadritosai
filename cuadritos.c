#include <stdio.h>
#include <stdlib.h>

#define SIZE 5
#define PLAYERS 2

typedef struct{
    short top;
    short right;
    short bottom;
    short left;
    short last;
    short value;
    short color;
} tile;

tile** BOARD;
int TOTAL=0;

int placeWall (int, int, int, tile**);
int colorTheBoard(int, int, int, tile**, char** grid);
int colorNeighbour(int, int, int, tile**, char** grid);
int initGrid(char**);
int putGrid(int, int, int, char**);
int printGrid(char**);


int main() {
    if ((BOARD = (tile**) malloc (SIZE*sizeof(tile*))) == NULL)
        return 1;
    for (int i=0; i<SIZE; i++){
        if ((*(BOARD+i) = (tile*) malloc (SIZE*sizeof(tile))) == NULL)
          return 1;
        for (int j=0; j<SIZE; j++){
          (*(*(BOARD+i)+j)).color=0;
        }
    }

    char** grid;
    if ((grid = (char**) malloc ((2*SIZE+1)*sizeof(char*)))==NULL)
      return 1;
    for(int i=0; i<2*SIZE+1; i++){
      if((*(grid+i) = (char*) malloc ((2*SIZE+1)*sizeof(char)))==NULL)
        return 1;
    }

    initGrid(grid);
    printGrid(grid);
    
    int cursor[3];
    int turn=0;

    printf("Input format: col|row|wall\n");
    do {
      printf("Player %d's turn:\t", turn+1);
      // scanf("%d %d %d", &cursor[0], &cursor[1], &cursor[2]);
      fscanf(stdin, "%d %d %d", &cursor[0], &cursor[1], &cursor[2]);
      system("clear");
      if(placeWall(cursor[0], cursor[1], cursor[2], BOARD)){
          putGrid(cursor[0], cursor[1], cursor[2], grid);
          printGrid(grid);
          continue; 
      }
      int changer = colorTheBoard(cursor[0], cursor[1], turn, BOARD, grid);
      putGrid(cursor[0], cursor[1], cursor[2], grid);
      printGrid(grid);
      if(changer==1)
        turn = (turn+1)%PLAYERS;
    } while(TOTAL!=SIZE*SIZE);

    puts("Executat amb exit\n");
    return 0;
}


int placeWall (int row, int col, int wall, tile** board){
    // WHICH VALUE SHOULD "wall" TAKE
    // 1 FOR TOP
    // 2 FOR RIGHT
    // 3 FOR BOTTOM
    // 4 FOR RIGHT
    
    tile current = *(*(board+col)+row);
    switch (wall){
        case 1:
            if (current.top == 1)
                return 1;
            (*(*(board+col)+row)).top=1;
            (*(*(board+col)+row)).last=wall;
            (*(*(board+col)+row)).value++;
            if (row!=0){
                (*(*(board+col)+row-1)).bottom=1;
                (*(*(board+col)+row-1)).value++;
            }
            break;
        case 2:
            if (current.right == 1)
                return 1;
            (*(*(board+col)+row)).right=1;
            (*(*(board+col)+row)).last=wall;
            (*(*(board+col)+row)).value++;
            if (col!=SIZE-1){
                (*(*(board+col+1)+row)).left=1;
                (*(*(board+col+1)+row)).value++;
            }
            break;
        case 3:
            if (current.bottom == 1)
                return 1;
            (*(*(board+col)+row)).bottom=1;
            (*(*(board+col)+row)).last=wall;
            (*(*(board+col)+row)).value++;
            if (row!=SIZE-1){
                (*(*(board+col)+row+1)).top=1;
                (*(*(board+col)+row+1)).value++;
            }
            break;
        case 4:
            if (current.left == 1)
                return 1;
            (*(*(board+col)+row)).left=1;
            (*(*(board+col)+row)).last=wall;
            (*(*(board+col)+row)).value++;
            if (col!=0){
                (*(*(board+col-1)+row)).right=1;
                (*(*(board+col-1)+row)).value++;
            }
            break;
        default:
            return 1;
    }
    return 0;
}


int colorTheBoard(int row, int col, int player, tile** board, char** grid){
    if ((*(*(board+col)+row)).value==4 && (*(*(board+col)+row)).color==0){
        (*(*(board+col)+row)).color=player+1; 
        *(*(grid+2*col+1)+2*row+1)=player+1;
        TOTAL+=1;
        switch ((*(*(board+col)+row)).last){
          case 1:
              if (row!=0)
                  colorNeighbour(col, row-1, player, board, grid);
              break;
          case 2:
              if(col!=SIZE-1)
                  colorNeighbour(col+1, row, player, board, grid);
              break;
          case 3:
              if(row!=SIZE-1)
                  colorNeighbour(col, row+1, player, board, grid);
              break;
          case 4:
              if(col!=0)
                colorNeighbour(col-1, row, player, board, grid);
              break;
          default:
              return 1;
        }
        return 0;
    }
    return 1;
}


int colorNeighbour(int row, int col, int player, tile** board, char** grid){
    tile current = (*(*(board+col)+row));
    if ((*(*(board+col)+row)).value==4 && (*(*(board+col)+row)).color==0){
        (*(*(board+col)+row)).color=player+1;
        *(*(grid+2*col+1)+2*row+1)=player+1;
        TOTAL+=1;
    } else if (current.value==3 && (*(*(board+col)+row)).color==0){
        if (current.top==0){
            placeWall(col, row, 1, board);
        } else if (current.right==0){
            placeWall(col, row, 2, board);
        } else if (current.bottom==0){
            placeWall(col, row, 3, board);
        } else if (current.left==0){
            placeWall(col, row, 4, board);
        }
        (*(*(board+col)+row)).color=player+1;
        *(*(grid+2*col+1)+2*row+1)=player+1;
        TOTAL+=1;
        switch ((*(*(board+col)+row)).last){
            case 1:
                if (row!=0)
                    colorNeighbour(col, row-1, player, board, grid);
                break;
            case 2:
                if(col!=SIZE-1)
                    colorNeighbour(col+1, row, player, board, grid);
                break;
            case 3:
                if(row!=SIZE-1)
                    colorNeighbour(col, row+1, player, board, grid);
                break;
            case 4:
                if(col!=0)
                    colorNeighbour(col-1, row, player, board, grid);
                break;
            default:
                break;
        }
        return 0;
    }
    return 0;
}


int initGrid(char** grid){
  // 0 for " "
  // 1 for "+"
  // 2 for "——"
  // 3 for "|"

  for (int i=0; i<2*SIZE+1; i++){
    switch (i%2) {
      case 0: 
        for (int j=0; j<2*SIZE+1; j++){
          switch (j%2){
            case 0:
              *(*(grid+i)+j)='1';
              break;
            case 1:
              *(*(grid+i)+j)='0';
              break;
          }
        }
        break;
      case 1:
        for(int j=0; j<2*SIZE+1;j++){
          *(*(grid+i)+j)='0';
        }
        break;
    }
  }
  return 0;
}

int putGrid(int row, int col, int wall, char** grid){
  switch (wall){
    case 1:
      *(*(grid+2*col+1)+2*row)='2';
      break;
    case 3:
      *(*(grid+2*col+1)+2*(row+1))='2';
      break;
    case 2:
      *(*(grid+2*(col+1))+2*row+1)='3';
      break;
    case 4:
      *(*(grid+2*col)+2*row+1)='3';
    default:
      break;
  }
  return 0;
}

int printGrid(char** grid){
  printf("\n\n\n");
  for(int i=0; i<2*SIZE+1; i++){
    for(int j=0; j<2*SIZE+1; j++){
      switch(*(*(grid+j)+i)){
        case '0':
          if(j%2==0)
            printf(" ");
          if(j%2==1)
            printf("  ");
          break;
        case '1':
          printf("+");
          break;
        case '2':
          printf("——");
          break;
        case '3':
          printf("|");
          break;
        default:
          printf("%d ",*(*(grid+j)+i));
          break;
      }
    }
    printf("\n");
  }
  printf("\n\n\n");
  return 0;
}
