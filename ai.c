// ERRORS A CORREGIR:
// - ALGUNS QUADRATS ES COMPLETEN SENSE TENIR 4 MURS
// - A VEGADES LA PROPAGACIÓ NO ES FA CORRECTAMENT


#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <unistd.h>
#include <limits.h>
#include <time.h>
#include "assets/resources.c"

#define TILES 2*SIZE*(SIZE+1) // FOR EVERY REPRESENTABLE CHARACTER ON THE GRID
#define popSize 20  // MUST BE EVEN SO genAlg CAN COMPARE INDIVIDUALS 1 ON 1
#define GENERATIONS 100


typedef struct {
  short W[TILES][TILES];
  short B[TILES];
} individual;

typedef struct {
  individual individual[popSize];
} population;


void randomGen(population* newGen);
int geneticAlg(population* oldGen, population* newGen, short mutationProb);
void mutate(individual* individual, short mutationProb);
int eval(individual* individual, short* state);
void indexToPlay(int index, int play[3]);
int playToIndex(int play[3]);
double lossFunction(int index);



int main() {

  int* TOTAL = (int*) malloc (sizeof(int));
  *TOTAL = 0;

  // GENEREM LA POBLACIÓ
  
  population* initialGen;
  if((initialGen = (population*) malloc(sizeof(population)))==NULL)
    return 0;
  randomGen(initialGen);


      tile** BOARD;
      if ((BOARD = (tile**) malloc (SIZE*sizeof(tile*))) == NULL)
          return 1;
      for (int i=0; i<SIZE; i++){
          if ((*(BOARD+i) = (tile*) malloc (SIZE*sizeof(tile))) == NULL)
            return 1;
          for (int j=0; j<SIZE; j++){
            (*(*(BOARD+i)+j)).top=0;
            (*(*(BOARD+i)+j)).right=0;
            (*(*(BOARD+i)+j)).bottom=0;
            (*(*(BOARD+i)+j)).left=0;
            (*(*(BOARD+i)+j)).last=0;
            (*(*(BOARD+i)+j)).value=0;
            (*(*(BOARD+i)+j)).color=0;
          }
      }
      char** grid;
      if ((grid = (char**) malloc ((2*SIZE+1)*sizeof(char*)))==NULL)
        return 1;
      for(int i=0; i<2*SIZE+1; i++){
        if((*(grid+i) = (char*) malloc ((2*SIZE+1)*sizeof(char)))==NULL)
          return 1;
      }
      initGrid(grid);
      printGrid(grid);

      // FI INICIALITZACIÓ

      
      int cursor[3];  // COORDENADES DE LA JUGADA (col, row, tile)
      int turn=0; // TORN 
      short index; // JUGADES AVALUADES PER LA IA EN FORMA DE NUMERO (NO DE COORDENADES)


      short* state; // PASSEM L'ESTAT INICIAL DEL TAULER (tot zeros) EN FORMA DE VECTOR
      if((state=(short*) malloc (TILES*sizeof(short)))==NULL)
        return 1;
      for(int i=0; i<TILES; i++){*(state+i)=0;}
      int result=0;


      individual Player[2] = {initialGen->individual[0], initialGen->individual[1]};
      // JUGARAN DOS JUGADORS DE LA POBLACIÓ
      
      do {
        system("clear");
        if(turn==0){
          index = eval(&Player[turn], state); // S'AVALUA EL TAULER I ES
                                                    // RETORNA UNA JUGADA index
          *(state+index)=1; // S'ACTUALITZA EL QUE LLEGEIX LA IA AMB AQUEST NOU INDEX
                            // CALDRIA AFEGIR UN BUCLE PER SI LA JUGADA NO ES POSSIBLE
          indexToPlay(index, cursor); // ES PASSA A FORMA D'INDEX
          placeWall(cursor[0], cursor[1], cursor[2], BOARD);
          printf("AI %d played %d %d %d\n", turn, cursor[0], cursor[1], cursor[2]);
        } else if (turn==1){
          index = eval(&Player[turn], state);
          *(state+index)=1;
          indexToPlay(index, cursor);
          placeWall(cursor[0], cursor[1], cursor[2], BOARD);
          printf("AI %d played %d %d %d\n", turn, cursor[0], cursor[1], cursor[2]);
        }

        int changer = colorTheBoard(cursor[0], cursor[1], turn, BOARD, grid, TOTAL);  // SI RETORNA 1 ÉS QUE NO HA
                                                                                      // PASSAT RES
        for(int i=0; i<SIZE; i++){
          for (int j=0; j<SIZE; j++){
            printf("%d\t", (*(*(BOARD+i)+j)).color);
          }
          puts("\n");
        } 

        putGrid(cursor[0], cursor[1], cursor[2], grid);
        printGrid(grid);
        if(changer==1)
          turn = (turn+1)%PLAYERS;
        printf("TOTAL:\t%d\n", *TOTAL);
        sleep(1);
      } while(*TOTAL!=SIZE*SIZE);
                 
      //TOTAL=0; // REINICIEM EL COMPTADOR PER LA SEGÜENT PARTIDA


      // VEGEM QUI DELS DOS HA GUANYAT

      for(int i=0; i<SIZE; i++){ for (int j=0; j<SIZE; j++){
        if((*(*(BOARD+i)+j)).color==1){
          result+=1;
        } else if((*(*(BOARD+i)+j)).color==2){
          result-=1;
      }}}

      free(BOARD);
      free(grid);
  return 1;
}



void indexToPlay(int index, int* play){
  if (index>=SIZE*(SIZE+1)){
    index-=SIZE*(SIZE+1);
    *(play+2)=1;
    *(play+1)=index%SIZE;
    int row=0;
    while((index-=SIZE) >=0)
      row+=1;
    *play=row;
    if (*play==SIZE){
      *(play+2) = 3;
      *play = SIZE-1;
    }
  } else {
    *(play+1) = index%(SIZE+1);
    int row=0;
    while((index-=(SIZE+1)) >=0)
      row+=1;
    *(play)=row;
    *(play+2)=4;
    if (*(play+1)==SIZE){
      *(play+2)=2;
      *(play+1)-=1;
    }
  }
  return;
}

int playToIndex(int play[3]){
  int index=0;
  switch (play[2]) {
    case 1:
      index+=30+play[0]*SIZE+play[1];
      break;
    case 3:
      index+=30+(play[0]+1)*SIZE+play[1];
      break;
    case 4:
      index+=play[0]*(SIZE+1)+play[1];
      break;
    case 2:
      index+=play[0]*(SIZE+1)+(play[1]+1);
      break;
    default:
      return -1;
  }
  return index;
}


double lossFunction(int index){   // FORMARÀ PART DE LA FUNCIÓ eval
  int play[3];
  indexToPlay(index, play);
  printf("Row\t%d\nCol\t%d\nWall\t%d\n\n\n", play[0], play[1], play[2]);
  return 1;
}


int eval(individual* individual, short* state){   // AVALUA LA POSICIÓ I RETORNA LA JUGADA ÒPTIMA
  short optIndex; // MILLOR JUGADA (index amb major fitness)
  optIndex =0;
  short linear[TILES]; // FITNESS DE CADA INDEX
  for (int i=0; i<TILES; i++){
    for (int j=0; j<TILES; j++){
      *(linear+i)+=individual->W[i][j]*(*(state+j));
    }
    *(linear+i)+= individual->B[i];
  }
  for (int i=1; i<TILES; i++){  // ES TÉ EN COMPTE SI UNA JUGADA ÉS REPETIDA (NO ÉS POSSIBLE)
    if(linear[i]>linear[optIndex] && *(state+i)!=1)
      optIndex=i;
  }

  //lossFunction(optIndex);
  return optIndex;
}


void randomGen(population* newGen){   // GENERACIÓ ALEATÒRIA D'UNA NOVA POBLACIÓ
  for (int indCount=0;indCount<popSize; indCount++){
    for (int i=0; i<TILES; i++){
      for (int j=0; j<TILES; j++){
        newGen->individual[indCount].W[i][j] = rand()-RAND_MAX/2;
      }
    newGen->individual[indCount].B[i]=rand()-RAND_MAX/2;
    }
  }
}




/*
void mutate(individual* individual, short mutationProb){
  float threshold = (float)RAND_MAX; 
  for (int i=0; i<TILES; i++){
    if (threshold<rand())
      individual->index[i] = round((float)individual->index[i] * mutationProb);
  }
}


int geneticAlg(population* oldGen, population* newGen, short mutationProb){
  for (int i=0; i<popSize/2; i++){
    continue;
  }
  return 0;
}


*/
