import pygame as pg
import sys, time
from subprocess import call
from struct import unpack


SIZE = 5
tileSizeBytes = 14
PLAYERS = 2
dotRadius = 10
rectLen = 100
rectWidth = 2*dotRadius
rectAndDot = rectLen+2*dotRadius
windowSize = (SIZE+1)*2*dotRadius+SIZE*rectLen
backgroundColor=(253,244,220)
backgroundColors = [(255, 127, 127), (127, 127, 255), (127, 255, 127)]
inactiveColor = (238, 219, 207)
fillerColors = [(210, 65, 65), (65, 65, 210), (65, 210, 65)]
activeColors = [(255, 51, 51), (51, 51, 255), (51, 255, 51)]
cornerColor = (56, 55, 54)
textColor=(10, 10, 10)
cDataFile = "data.bin"


class button():
    def __init__(self, x, y, width, height):
        self.rect = pg.Rect(x, y, width, height)
        self.color = inactiveColor
        self.action = False

    def activated(self, surface, player):
        pos = pg.mouse.get_pos()
        if self.rect.collidepoint(pos):
            if pg.mouse.get_pressed()[0] == 1 and self.action==False:
                self.color = activeColors[player]#cornerColor
                self.action = True
                return True
        if pg.mouse.get_pressed()[0] == 0:
            return False

class tile():
    def __init__(self, x, y, width):
        self.rect = pg.Rect(x, y, width, width)
        self.color=backgroundColor
        self.colored=False

    def colorTile(self, surface, player):
        self.color=fillerColors[player] 
        self.colored=True
        return True
    

def main():
    pg.init()
    scores = [0 for i in range(PLAYERS)]
    SCREEN = pg.display.set_mode((windowSize, windowSize*1.2))
    SCREEN.fill(backgroundColor)
    pg.display.set_caption("Cuadritos")

    # TEXT INFERIOR
    my_font=pg.font.SysFont('Comic sans MS', round(0.15*windowSize))
    score_font=pg.font.SysFont('Comic sans MS', round(0.05*windowSize))
    winner_font=pg.font.SysFont('Comic Sans MS', round(0.21*windowSize))


    # DISPLAY DE TOTS ELS CERCLES I RECTANGLES
    butV = []
    butH = []
    tiles = []

    for x in range(SIZE+1):
        for y in range(SIZE+1):
            pg.draw.circle(SCREEN, cornerColor, (dotRadius+x*rectAndDot, dotRadius+y*rectAndDot), dotRadius)
    for x in range(SIZE+1):
        for y in range(SIZE):
            tempH = button(rectWidth+rectAndDot*y, rectAndDot*x, rectLen, rectWidth)
            tempV = button(rectAndDot*x, rectWidth+rectAndDot*y, rectWidth, rectLen)
            butH.append(tempH)
            butV.append(tempV)
            pg.draw.rect(SCREEN, inactiveColor, tempH)
            pg.draw.rect(SCREEN, inactiveColor, tempV)
    for x in range(SIZE):
        for y in range(SIZE):
            temp = tile(rectWidth+rectAndDot*x, rectWidth+rectAndDot*y, rectLen)        
            tiles.append(temp)
            pg.draw.rect(SCREEN, backgroundColor, temp)


    # MAIN LOOP DEL PROGRAMA
    call(["./calc"])
    turn=0
    run=True
    total=0

    while run:
        for event in pg.event.get():
            if event.type == pg.QUIT:
                pg.quit()
                sys.exit()

        trigger=0

        for row in range(SIZE+1):
            for col in range(SIZE):
                index = row*SIZE+col
                if (butH[index]).activated(SCREEN, turn)==True:
                    turn=(turn+1)%PLAYERS
                    pg.draw.rect(SCREEN, (butH[index]).color, butH[index])
                    if row==SIZE:
                        if(call(["./calc", str(col), str(row-1), "3", str(turn)])==0):
                            trigger+=1
                    elif row==0:
                        if(call(["./calc", str(col), str(row), "1", str(turn)])==0):
                            trigger+=1
                    else:
                        if (call(["./calc", str(col), str(row), "1", str(turn)])==0):
                            trigger+=1
                        if(call(["./calc", str(col), str(row-1), "3", str(turn)])==0):
                            trigger+=1

        for col in range(SIZE+1):
            for row in range(SIZE):
                index = row+col*SIZE
                if (butV[index]).activated(SCREEN, turn)==True:
                    turn=(turn+1)%PLAYERS
                    pg.draw.rect(SCREEN, (butV[index]).color, butV[index])
                    if col==SIZE:
                        if(call(["./calc", str(col-1), str(row), "2", str(turn)])==0):
                            trigger+=1
                    elif col==0:
                        if(call(["./calc", str(col), str(row), "4", str(turn)])==0):
                            trigger+=1
                    else:
                        if(call(["./calc", str(col), str(row), "4", str(turn)])==0):
                            trigger+=1
                        if(call(["./calc", str(col-1), str(row), "2", str(turn)])==0):
                            trigger+=1

        tilesRead=[]
        fhand = open(cDataFile, "rb")
        while True:
            try:
                buffer = fhand.read(tileSizeBytes)
                tilesRead.append(unpack('14B', buffer)[-4])
            except:
                break
        fhand.close()
       
        for i in range(SIZE):
            for j in range(SIZE):
                index=i*SIZE+j
                if tilesRead[index]==4 and tiles[index].colored==False:
                    tiles[index].colorTile(SCREEN, (turn-1)%PLAYERS)
                    pg.draw.rect(SCREEN, tiles[index].color, tiles[index])
                    total+=1
                    scores[(turn-1)%PLAYERS]+=1

        if trigger!=0:
            turn = (turn-1)%PLAYERS

        pg.draw.rect(SCREEN, backgroundColors[turn], (0, windowSize, windowSize, (0.2*windowSize)))

        for i in range(PLAYERS):
            scoreText = score_font.render("Player {num}: {score}".format(num=i+1, score=scores[i]), True, textColor)
            SCREEN.blit(scoreText, (round(windowSize*0.8), windowSize*(1+0.05*i)))
        turnRect=my_font.render("Player {turn}'s turn".format(turn=turn+1), True, textColor)
        SCREEN.blit(turnRect, (0, windowSize))

        pg.display.update()
        
        if total==SIZE*SIZE:
            winner=scores.index(max(scores))
            pg.draw.rect(SCREEN, backgroundColors[winner], (0, 0, windowSize, (1.2*windowSize)))
            winnerRect=winner_font.render("Player {winner} wins!".format(winner=winner+1), True, textColor)
            SCREEN.blit(winnerRect, (0, round(windowSize*0.5)))
            pg.display.update()
            time.sleep(3)
            for i in range(PLAYERS):
                print("Player {player}'s score:\t{score}\n".format(player=i+1, score=scores[i]))
            run=False
        

if __name__== "__main__":
    main()
