#include <stdio.h>
#include <stdlib.h>
#define SIZE 5
#define PLAYERS 2
typedef struct{
    short top;
    short right;
    short bottom;
    short left;
    short last;
    short value;
    short color;
} tile;


int placeWall (int row, int col, int wall, tile** board){
    // WHICH VALUE SHOULD "wall" TAKE
    // 1 FOR TOP
    // 2 FOR RIGHT
    // 3 FOR BOTTOM
    // 4 FOR RIGHT
    
    tile current = *(*(board+row)+col);
    switch (wall){
        case 1:
            if (current.top == 1)
                return 1;
            (*(*(board+row)+col)).top=1;
            (*(*(board+row)+col)).last=wall;
            (*(*(board+row)+col)).value++;
            if (row!=0){
                (*(*(board+row-1)+col)).bottom=1;
                (*(*(board+row-1)+col)).last=3;
                (*(*(board+row-1)+col)).value++;
            }
            break;
        case 2:
            if (current.right == 1)
                return 1;
            (*(*(board+row)+col)).right=1;
            (*(*(board+row)+col)).last=wall;
            (*(*(board+row)+col)).value++;
            if (col!=SIZE-1){
                (*(*(board+row)+col+1)).left=1;
                (*(*(board+row)+col+1)).last=4;
                (*(*(board+row)+col+1)).value++;
            }
            break;
        case 3:
            if (current.bottom == 1)
                return 1;
            (*(*(board+row)+col)).bottom=1;
            (*(*(board+row)+col)).last=wall;
            (*(*(board+row)+col)).value++;
            if (row!=SIZE-1){
                (*(*(board+row+1)+col)).top=1;
                (*(*(board+row+1)+col)).last=1;
                (*(*(board+row+1)+col)).value++;
            }
            break;
        case 4:
            if (current.left == 1)
                return 1;
            (*(*(board+row)+col)).left=1;
            (*(*(board+row)+col)).last=wall;
            (*(*(board+row)+col)).value++;
            if (col!=0){
                (*(*(board+row)+col-1)).right=1;
                (*(*(board+row)+col-1)).last=2;
                (*(*(board+row)+col-1)).value++;
            }
            break;
        default:
            return 1;
    }
    return 0;
}


int colorNeighbour(int row, int col, int player, tile** board, char** grid, int* TOTAL){
    if ((*(*(board+row)+col)).value==4 && (*(*(board+row)+col)).color==0){
        //printf("Called colorNeighbour on %d %d via end\n", row, col);
        (*(*(board+row)+col)).color=player+1;

        // S'HA CANVIAT L'ORDRE
        //*(*(grid+2*col+1)+2*row+1)=player+1;
        *(*(grid+2*row+1)+2*col+1)=player+1;

        *TOTAL+=1;
    } else if ((*(*(board+row)+col)).value==3 && (*(*(board+row)+col)).color==0){
        //printf("Called colorNeighbour on %d %d via next\n", row, col);
        if ((*(*(board+row)+col)).top==0){
            placeWall(row, col, 1, board);
        } else if ((*(*(board+row)+col)).right==0){
            placeWall(row, col, 2, board);
        } else if ((*(*(board+row)+col)).bottom==0){
            placeWall(row, col, 3, board);
        } else if ((*(*(board+row)+col)).left==0){
            placeWall(row, col, 4, board);
        }
        (*(*(board+row)+col)).color=player+1;

        // S'HA CANVIAT L'ORDRE
        //*(*(grid+2*col+1)+2*row+1)=player+1;
        *(*(grid+2*row+1)+2*col+1)=player+1;

        *TOTAL+=1;
        switch ((*(*(board+row)+col)).last){
            case 1:
                if (row!=0)
                    colorNeighbour(row-1, col, player, board, grid, TOTAL);
                break;
            case 2:
                if(col!=SIZE-1)
                    colorNeighbour(row, col+1, player, board, grid, TOTAL);
                break;
            case 3:
                if(row!=SIZE-1)
                    colorNeighbour(row+1, col, player, board, grid, TOTAL);
                break;
            case 4:
                if(col!=0)
                    colorNeighbour(row, col-1, player, board, grid, TOTAL);
                break;
            default:
                break;
        }
        return 0;
    }
    return 0;
}

int colorTheBoard(int row, int col, int player, tile** board, char** grid, int* TOTAL){
    if ((*(*(board+row)+col)).value==4 && (*(*(board+row)+col)).color==0){
        //printf("Called colorTheBoard on %d %d\n", row, col);
        (*(*(board+row)+col)).color=player+1; 

        // S'HA CANVIAT L'ORDRE
        //*(*(grid+2*col+1)+2*row+1)=player+1;
        *(*(grid+2*row+1)+2*col+1)=player+1;

        *TOTAL+=1;
        switch ((*(*(board+row)+col)).last){
          case 1:
              if (row!=0)
                  colorNeighbour(row-1, col, player, board, grid, TOTAL);
              break;
          case 2:
              if(col!=SIZE-1)
                  colorNeighbour(row, col+1, player, board, grid, TOTAL);
              break;
          case 3:
              if(row!=SIZE-1)
                  colorNeighbour(row+1, col, player, board, grid, TOTAL);
              break;
          case 4:
              if(col!=0)
                colorNeighbour(row, col-1, player, board, grid, TOTAL);
              break;
          default:
              return 0; // ABANS RETURN 1
        }
        return 0;
    }
    return 1; // QUAN NO PASSA RES ES FA RETURN 1
}

int initGrid(char** grid){
  // 0 for " "
  // 1 for "+"
  // 2 for "——"
  // 3 for "|"

  for (int i=0; i<2*SIZE+1; i++){
    switch (i%2) {
      case 0: 
        for (int j=0; j<2*SIZE+1; j++){
          switch (j%2){
            case 0:
              *(*(grid+i)+j)='1';
              break;
            case 1:
              *(*(grid+i)+j)='0';
              break;
          }
        }
        break;
      case 1:
        for(int j=0; j<2*SIZE+1;j++){
          *(*(grid+i)+j)='0';
        }
        break;
    }
  }
  return 0;
}

int putGrid(int row, int col, int wall, char** grid){ // POSAR UN MUR NOU AL GRID
  switch (wall){
    case 1:
      *(*(grid+2*col+1)+2*row)='2';
      break;
    case 3:
      *(*(grid+2*col+1)+2*(row+1))='2';
      break;
    case 2:
      *(*(grid+2*(col+1))+2*row+1)='3';
      break;
    case 4:
      *(*(grid+2*col)+2*row+1)='3';
    default:
      break;
  }
  return 0;
}

int printGrid(char** grid){ // IMPRIMIR LA GRID
  printf("\n\n\n");
  for(int i=0; i<2*SIZE+1; i++){
    for(int j=0; j<2*SIZE+1; j++){
      switch(*(*(grid+j)+i)){
        case '0':
          if(j%2==0)
            printf(" ");
          if(j%2==1)
            printf("  ");
          break;
        case '1':
          printf("+");
          break;
        case '2':
          printf("——");
          break;
        case '3':
          printf("|");
          break;
        default:
          printf("%d ",*(*(grid+j)+i));
          break;
      }
    }
    printf("\n");
  }
  printf("\n\n\n");
  return 0;
}
