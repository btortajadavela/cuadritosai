#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <unistd.h>
#include <limits.h>
#include <time.h>
#include "assets/resources.c"

//#define SIZE 5
#define TILES 2*SIZE*(SIZE+1)
#define popSize 20  // MUST BE EVEN SO genAlg CAN COMPARE INDIVIDUALS 1 ON 1
#define GENERATIONS 100


typedef struct {
  short W[TILES][TILES];
  short B[TILES];
} individual;

typedef struct {
  individual individual[popSize];
} population;



void randomGen(population* newGen); // DONE
int geneticAlg(population* oldGen, population* newGen, short mutationProb);
void mutate(individual* individual, short mutationProb);  // DONE
int eval(individual* individual, short* state);
void indexToPlay(int index, int play[3]);
int playToIndex(int play[3]);
double lossFunction(int index);



int main() {
  
  population* initialGen;
  if((initialGen = (population*) malloc(sizeof(population)))==NULL)
    return 0;

  
  randomGen(initialGen);

  for (int rounds=0; rounds<popSize/2; rounds++){
      tile** BOARD;
      if ((BOARD = (tile**) malloc (SIZE*sizeof(tile*))) == NULL)
          return 1;
      for (int i=0; i<SIZE; i++){
          if ((*(BOARD+i) = (tile*) malloc (SIZE*sizeof(tile))) == NULL)
            return 1;
          for (int j=0; j<SIZE; j++){
            (*(*(BOARD+i)+j)).color=0;
          }
      }
      char** grid;
      if ((grid = (char**) malloc ((2*SIZE+1)*sizeof(char*)))==NULL)
        return 1;
      for(int i=0; i<2*SIZE+1; i++){
        if((*(grid+i) = (char*) malloc ((2*SIZE+1)*sizeof(char)))==NULL)
          return 1;
      }
      initGrid(grid);
      printGrid(grid);

      
      int cursor[3];
      int turn=0;
      short* state;
      if((state=(short*) malloc (TILES*sizeof(short)))==NULL)
        return 1;
      for(int i=0; i<TILES; i++){*(state+i)=0;}
      int result=0;
      individual Player[2] = {initialGen->individual[rounds], initialGen->individual[rounds+1]};

      do {
        // PART AFEGIDA PER FER LA DECISIÓ ENTRE IAS
        int index = eval(&Player[turn], state);
        state[index]=1;
        indexToPlay(index, cursor);

        if(placeWall(cursor[0], cursor[1], cursor[2], BOARD)){
            putGrid(cursor[0], cursor[1], cursor[2], grid);
            printGrid(grid);
            continue; 
        }
        int changer = colorTheBoard(cursor[0], cursor[1], turn, BOARD, grid);
        putGrid(cursor[0], cursor[1], cursor[2], grid);
        printGrid(grid);
        if(changer==1)
          turn = (turn+1)%PLAYERS;

        sleep(1);
        system("clear");
      } while(TOTAL!=SIZE*SIZE);


      // VEGEM QUI DELS DOS HA GUANYAT

      for(int i=0; i<SIZE; i++){ for (int j=0; j<SIZE; j++){
        if((*(*(BOARD+i)+j)).color==1){
          result+=1;
        } else if((*(*(BOARD+i)+j)).color==2){
          result-=1;
      }}}

      free(BOARD);
      free(grid);
  }
  return 1;
}



void indexToPlay(int index, int* play){
  if (index>=SIZE*(SIZE+1)){
    index-=SIZE*(SIZE+1);
    int module = index%(SIZE+1);
    *(play+2)=4;
    *(play+1)=module;
    if (module == SIZE){
      *(play+2) = 2;
      *(play+1) = SIZE-1;
    }
    int rowTemp=0;
    while((index-=SIZE+1) >=0)
      rowTemp+=1;
    *play = rowTemp;

  } else {
    *(play+1) = index%SIZE;
    int rowTemp=0;
    while((index-=SIZE) >=0)
      rowTemp+=1;
    *(play)=rowTemp;
    *(play+2)=1;
    if (rowTemp==SIZE){
      *(play+2)=3;
      *(play)=rowTemp-1;
    }
  }
  return;
}

int playToIndex(int play[3]){
  return 1;
}

double lossFunction(int index){
  int play[3];
  indexToPlay(index, play);
  printf("Row\t%d\nCol\t%d\nWall\t%d\n\n\n", play[0], play[1], play[2]);
  return 1;
}




int eval(individual* individual, short* state){
  int optIndex=0;
  short linear[TILES];
  for (int i=0; i<TILES; i++){
    for (int j=0; j<TILES; j++){
      *(linear+i)+=individual->W[i][j]*(*(state+j));
    }
    *(linear+i)+= individual->B[i];
    for (int i=0; i<TILES; i++){
      if(linear[i]>linear[optIndex])
        optIndex=i;
    }
  //lossFunction(optIndex);
  }
  return optIndex;
}


void randomGen(population* newGen){
  for (int indCount=0;indCount<popSize; indCount++){
    for (int i=0; i<TILES; i++){
      for (int j=0; j<TILES; j++){
        newGen->individual[indCount].W[i][j] = rand();
      }
    newGen->individual[indCount].B[i]=rand();
    }
  }
}

/*
void mutate(individual* individual, short mutationProb){
  float threshold = (float)RAND_MAX; 
  for (int i=0; i<TILES; i++){
    if (threshold<rand())
      individual->index[i] = round((float)individual->index[i] * mutationProb);
  }
}


int geneticAlg(population* oldGen, population* newGen, short mutationProb){
  for (int i=0; i<popSize/2; i++){
    continue;
  }
  return 0;
}


*/
