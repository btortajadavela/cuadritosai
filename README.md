# cuadritosAI



## Getting started

Aquest projecte pretén crear una inteligència artificial que aprengui a jugar al joc dels "quadritos". Primer de tot, caldrà crear aquest joc i adaptar-lo.

## Roadmap

- [x] Crear el joc dels quadritos en C
- [x] Passar el joc a un mòdul de C
- [x] Crear una interfície gràfica en Python que faci servir el mòdul com a nucli del joc
- [x] Adaptar el mòdul per a que puguin jugar-hi dues IAs, l'una contra l'altra 

        - Fet, tot i que hi ha algun bug en la propagació de quadritos que cal arreglar.
        
- [ ] Crear un algoritme genètic que faci millorar les IAs
- [ ] Entrenar les IAs fins que siguin invencibles
- [ ] Adaptar la interfície gràfica perquè es pugui jugar humà vs. màquina
