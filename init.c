#include <stdio.h>
#include <stdlib.h>
#define SIZE 5
#define PLAYERS 2

typedef struct{
    short top;short right;short bottom;short left;
    short last;
    short value;
    short color;
} tile;

tile defaultTile = {0, 0, 0, 0, 0, 0, 0};

tile** BOARD;
int TOTAL=0;
char fileName[]="data.bin";

int placeWall (int, int, int, tile**);
int colorTheBoard(int, int, int, tile**);
int colorNeighbour(int, int, int, tile**);


int placeWall (int col, int row, int wall, tile** board){
    // WHICH VALUE SHOULD "wall" TAKE
    // 1 FOR TOP
    // 2 FOR RIGHT
    // 3 FOR BOTTOM
    // 4 FOR RIGHT
    
    tile current = *(*(board+col)+row);
    switch (wall){
        case 1:
            if (current.top == 1)
                return 1;
            (*(*(board+col)+row)).top=1;
            (*(*(board+col)+row)).last=wall;
            (*(*(board+col)+row)).value++;
            if (row!=0){
                (*(*(board+col)+row-1)).bottom=1;
                (*(board+col)+row-1)->last=3;
                (*(*(board+col)+row-1)).value++;
            }
            break;
        case 2:
            if (current.right == 1)
                return 1;
            (*(*(board+col)+row)).right=1;
            (*(*(board+col)+row)).last=wall;
            (*(*(board+col)+row)).value++;
            if (col!=SIZE-1){
                (*(*(board+col+1)+row)).left=1;
                (*(board+col+1)+row)->last=4;
                (*(*(board+col+1)+row)).value++;
            }
            break;
        case 3:
            if (current.bottom == 1)
                return 1;
            (*(*(board+col)+row)).bottom=1;
            (*(*(board+col)+row)).last=wall;
            (*(*(board+col)+row)).value++;
            if (row!=SIZE-1){
                (*(*(board+col)+row+1)).top=1;
                (*(board+col)+row+1)->last=1;
                (*(*(board+col)+row+1)).value++;
            }
            break;
        case 4:
            if (current.left == 1)
                return 1;
            (*(*(board+col)+row)).left=1;
            (*(*(board+col)+row)).last=wall;
            (*(*(board+col)+row)).value++;
            if (col!=0){
                (*(*(board+col-1)+row)).right=1;
                (*(board+col-1)+row)->last=2;
                (*(*(board+col-1)+row)).value++;
            }
            break;
        default:
            return 1;
    }
    return 0;
}


int colorTheBoard(int col, int row, int player, tile** board){
    if ((*(*(board+col)+row)).value==4){ 
        (*(*(board+col)+row)).color=player+1;
        TOTAL+=1;
        switch ((*(*(board+col)+row)).last){
            case 1:
                if (row!=0)
                    colorNeighbour(col, row-1, player, board);
                break;
            case 2:
                if(col!=SIZE-1)
                    colorNeighbour(col+1, row, player, board);
                break;
            case 3:
                if(row!=SIZE-1)
                    colorNeighbour(col, row+1, player, board);
                break;
            case 4:
                if(col!=0)
                    colorNeighbour(col-1, row, player, board);
                break;
            default:
                return 0;
        }
        return 0;
    }
    return 1;
}


int colorNeighbour(int col, int row, int player, tile** board){
    if ((*(board+col)+row)->value==4 && (*(board+col)+row)->color==0){
      colorTheBoard(col, row, player, board);
    } else if((*(board+col)+row)->value==3 && (*(board+col)+row)->color==0){
        if ((*(board+col)+row)->top==0){
            placeWall(col, row, 1, board);
        } else if ((*(board+col)+row)->right==0){
            placeWall(col, row, 2, board);
        } else if ((*(board+col)+row)->bottom==0){
            placeWall(col, row, 3, board);
        } else if ((*(board+col)+row)->left==0){
            placeWall(col, row, 4, board);
        }
        (*(board+col)+row)->color=player+1;
        TOTAL+=1;
        switch ((*(board+col)+row)->last){
            case 1:
                if (row!=0)
                    colorNeighbour(col, row-1, player, board);
                break;
            case 2:
                if(col!=SIZE-1)
                    colorNeighbour(col+1, row, player, board);
                break;
            case 3:
                if(row!=SIZE-1)
                    colorNeighbour(col, row+1, player, board);
                break;
            case 4:
                if(col!=0)
                    colorNeighbour(col-1, row, player, board);
                break;
            default:
                break;
        }
        return 0;
    }
    return 0;
}
